$(document).ready(function () {
  var $wrap = $('[js-ui-search]');
  var $close = $('[js-ui-close]');
  var $input = $('[js-ui-text]');

  $close.on('click', function () {
    $wrap.toggleClass('open');
  });

  $input.on('transitionend webkitTransitionEnd oTransitionEnd', function () {
    console.log('triggered end animation');
    if ($wrap.hasClass('open')) {
      $input.focus();
    } else {
      return;
    }
  });
  //Setup wow js
  var wow = new WOW({
    boxClass: 'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0,          // distance to the element when triggering the animation (default is 0)
    mobile: true,       // trigger animations on mobile devices (default is true)
    live: true,       // act on asynchronously loaded content (default is true)
    callback: function (box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  });
  wow.init();
  $('.data').slick(
    {
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }]
    });

  $("#myAbout").on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
      $('body').addClass('modal-open');
    }
  });

  $("#myCard").on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
      $('body').addClass('modal-open');
    }
  });

  $("#myContact").on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
      $('body').addClass('modal-open');
    }
  });
});